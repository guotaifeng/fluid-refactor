package com.paic.arch.jmsbroker;

/**
 * 测试或调用常量
 * @author guotaifeng
 *
 */
public class Constant {
	protected static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";
	public static final String TEST_QUEUE = "MY_TEST_QUEUE";
    public static final String MESSAGE_CONTENT = "Lorem blah blah";
	public static String REMOTE_BROKER_URL;
	
	public static JmsMessageBroker JMS_SUPPORT;
}
