package com.paic.arch.jmsbroker;

/**
 * 将测试或调用的动作封装，动态创建动作类
 * @author guotaifeng
 *
 */
public interface JmsMessage  {

	public JmsMessageBroker createARunningEmbeddedBrokerOnAvailablePort() throws Exception;
	
	public long sendsMessagesToTheRunningBroker() throws Exception;
	
	public String readsMessagesPreviouslyWrittenToAQueue() throws Exception;
	
	public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception;
	

}
