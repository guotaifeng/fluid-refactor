package com.paic.arch.jmsbroker;

import javax.jms.MessageProducer;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;
/**
 * 继承公共抽象类，实现可变操作接口
 * @author guotaifeng
 *
 */
public class JmsMessageBrokerSupport1  extends JmsMessageBrokerBase implements JmsMessageBroker{

	public final JmsMessageBrokerBase andThen() {
        return this;
    }
 
	protected JmsMessageBrokerSupport1(String aBrokerUrl) {
		super(aBrokerUrl);
	}

	@Override
	public JmsMessageBroker sendATextMessageToDestinationAt(String aDestinationName, String aMessageToSend) {
		executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageProducer producer = aSession.createProducer(aDestination);
            producer.send(aSession.createTextMessage(aMessageToSend));
            producer.close();
            return "";
        });
        return this;
	}
	
	 public static JmsMessageBrokerBase bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
	        return new JmsMessageBrokerSupport1(aBrokerUrl);
	 }
	 
     public static JmsMessageBrokerBase createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
    	 JmsMessageBrokerBase broker = bindToBrokerAtUrl(aBrokerUrl);
	     broker.createEmbeddedBroker();
	     broker.startEmbeddedBroker();
	     return broker;
	 }
     

	
	public static JmsMessageBrokerBase createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
	     return createARunningEmbeddedBrokerAt(DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000));
	 }

}
