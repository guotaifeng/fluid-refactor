package com.paic.arch.jmsbroker;

import static org.assertj.core.api.Assertions.assertThat;
/**
 * 对外提供一个访问，动态组装操作
 * @author guotaifeng
 *
 */
public class JmsMessageFacade {
	
    private static JmsMessage jmsMessage =new JmsMessageImpl();
	
    //protected static JmsMessage jmsProxy= (JmsMessageImpl) new JmsMessageBrokerProxy(jmsMessage).getProxyInstance();
	
	public void operate() {
    	
    		try {
    			
			((JmsMessageImpl) jmsMessage).setup();
			
    		assertThat(jmsMessage.sendsMessagesToTheRunningBroker()).isEqualTo(1);
			
    		assertThat(jmsMessage.readsMessagesPreviouslyWrittenToAQueue()).isEqualTo(((JmsMessageImpl) jmsMessage).MESSAGE_CONTENT);
			
    		jmsMessage.throwsExceptionWhenNoMessagesReceivedInTimeout();
			
    		((JmsMessageImpl) jmsMessage).teardown();
			
    		} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }
	
}
