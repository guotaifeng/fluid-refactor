package com.paic.arch.jmsbroker;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;
import static org.assertj.core.api.Assertions.assertThat;
import static org.slf4j.LoggerFactory.getLogger;

import org.slf4j.Logger;
/**
 * 测试或调用方法具体实现
 * @author guotaifeng
 *
 */
public class JmsMessageImpl extends Constant implements JmsMessage {

	private static final Logger LOG = getLogger(JmsMessageImpl.class);
	
	@Override
	public long sendsMessagesToTheRunningBroker() throws Exception {
		((JmsMessageBrokerSupport1) ((JmsMessageBrokerSupport1) JmsMessageBrokerSupport1.bindToBrokerAtUrl(REMOTE_BROKER_URL))
        .andThen()).sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT);
		long messageCount = ((JmsMessageBrokerBase) JMS_SUPPORT).getEnqueuedMessageCountAt(TEST_QUEUE);
        return messageCount;

	}

	@Override
	public String readsMessagesPreviouslyWrittenToAQueue() throws Exception {
		 String receivedMessage = ((JmsMessageBrokerSupport1) ((JmsMessageBrokerSupport1) JmsMessageBrokerSupport1.bindToBrokerAtUrl(REMOTE_BROKER_URL))
	                .sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT))
	                .andThen().retrieveASingleMessageFromTheDestination(TEST_QUEUE);
		 return receivedMessage;
	}

	@Override
	public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
		JmsMessageBrokerSupport1.bindToBrokerAtUrl(REMOTE_BROKER_URL).retrieveASingleMessageFromTheDestination(TEST_QUEUE, 1);
	}

	@Override
	public JmsMessageBroker createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
		return (JmsMessageBroker) createARunningEmbeddedBrokerAt(DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000));
	}
	
	 public static JmsMessageBrokerBase createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
	        LOG.debug("Creating a new broker at {}", aBrokerUrl);
	        JmsMessageBrokerBase broker = bindToBrokerAtUrl(aBrokerUrl);
	        broker.createEmbeddedBroker();
	        broker.startEmbeddedBroker();
	        return broker;
	}

    public static JmsMessageBrokerBase bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
        return new JmsMessageBrokerSupport1(aBrokerUrl);
    }
    
	public static void setup() throws Exception {
		JMS_SUPPORT = (JmsMessageBroker) JmsMessageBrokerSupport1.createARunningEmbeddedBrokerOnAvailablePort();
        REMOTE_BROKER_URL = ((JmsMessageBrokerBase) JMS_SUPPORT).getBrokerUrl();
    }
	
	public static void teardown() throws Exception {
        ((JmsMessageBrokerBase) JMS_SUPPORT).stopTheRunningBroker();
    }
}
