package com.paic.arch.jmsbroker;

import static org.slf4j.LoggerFactory.getLogger;

import org.slf4j.Logger;
/**
 * 接口 公用方法
 * @author guotaifeng
 *
 */
public interface JmsMessageBroker  {

    public JmsMessageBroker sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend);
    
}
